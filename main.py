from os import path, mkdir, chmod
from json import dump, load
from stat import S_IREAD, S_IRGRP, S_IROTH, UF_HIDDEN


def main():
    if path.exists('files/') is False:
        install()
    else:
        mkfile()


def install():
    print("Hi, my name is Apollo, I will save your files.")
    start = input('Do you want install me? [Yes or No]: ')
    if start.lower() == 'yes':
        name = input('Please enter your name: ')
        password = input('Please enter the password that you want to use: ')
        base = path.dirname(path.realpath(__file__))
        with open(path.join(base, 'database.json'), 'w') as db:
            data = {
                'data': {
                    'password': password,
                    'name': name
                }
            }
            dump(data, db)

        mkdir('files/')
    else:
        print("Oh! I'm so sad!")
        exit(0)


def mkfile():
    create = input("Hi, do you want to create a file? [Yes or No]: ")
    if create.lower() == 'yes':

        password = input("Insert your password: ")
        base = path.dirname(path.realpath(__file__))
        with open(path.join(base, 'database.json')) as db:
            data = load(db)
            if password == data['data']['password']:
                print("Welcome back " + data['data']['name'] + "!")
                file_name = input("Enter file name: ")
                text = input("Enter file content: ")
                with open('files/' + file_name + ".json", "w") as file:
                    data = {
                        file_name: {
                            'Content': text
                        }
                    }
                    dump(data, file)
                    chmod('files/' + file_name + '.json', S_IREAD | S_IRGRP | S_IROTH | UF_HIDDEN)

if __name__ == '__main__':
    main()
